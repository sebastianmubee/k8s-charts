# Example

```
apiVersion: helm.fluxcd.io/v1
kind: HelmRelease
metadata:
  name: traefik
  namespace: kube-system
  annotations:
    fluxcd.io/automated: "false"
    fluxcd.io/tag.chart-image: glob:v2.*
spec:
  releaseName: kibana
  helmVersion: v3
  chart:
    git: ssh://git@bitbucket.org/valtechar/valtech-k8s-charts.git
    ref: master
    path: system/traefik
  values:
    image:
      repository: traefik
      tag: "v2.2"

    ports:
      - name: http
        port: 80
        options:
          - forwardedHeaders.insecure

```


# With cert manager

First, cert manager CRD installation is needed:

```
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.15.0/cert-manager.yaml
```

```
apiVersion: helm.fluxcd.io/v1
kind: HelmRelease
metadata:
  name: traefik
  namespace: kube-system
  annotations:
    fluxcd.io/automated: "false"
    fluxcd.io/tag.chart-image: glob:v2.*
spec:
  releaseName: kibana
  helmVersion: v3
  chart:
    git: ssh://git@bitbucket.org/valtechar/valtech-k8s-charts.git
    ref: master
    path: system/traefik
  values:
    image:
      repository: traefik
      tag: "v2.2"

    ports:
      - name: http
        port: 80
        options:
          - forwardedHeaders.insecure
      - name: https
        port: 443
   
    certManager: true

---

ind: Namespace
apiVersion: v1
metadata: 
  name: cert-manager
  labels: 
    name: cert-manager

---

apiVersion: helm.fluxcd.io/v1
kind: HelmRelease
metadata:
  name: cert-manager
  namespace: cert-manager
  annotations:
    fluxcd.io/automated: "false"
spec:
  releaseName: cert-manager
  helmVersion: v3
  chart:
    repository: https://charts.jetstack.io
    name: cert-manager
    version: v0.15.0
  values:
    installCRDs: false

```

Also a certificate declaration is needed:

```
apiVersion: cert-manager.io/v1alpha2
kind: Certificate
metadata:
  name: example-com
  namespace: default
spec:
  secretName: example-com-tls
  issuerRef:
    name: letsencrypt-staging
    kind: ClusterIssuer
  dnsNames:
  - www.example.com
```
