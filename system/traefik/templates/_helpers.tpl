{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "traefik.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "traefik.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "traefik.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "traefik.labels" -}}
helm.sh/chart: {{ include "traefik.chart" . }}
{{ include "traefik.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "traefik.selectorLabels" -}}
app.kubernetes.io/name: {{ include "traefik.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Service Ports
*/}}
{{- define "traefik.servicePorts" -}}
{{- range .Values.ports }}
- name: {{ .name }}
  protocol: {{ default "TCP" .protocol }}
  port: {{ .port }}
{{- end }}
{{- if .Values.admin.enable }}
- name: admin
  protocol: TCP
  port: {{ default 8080 .Values.admin.port }}
{{- end }}
{{- end }}

{{/*
Container Ports
*/}}
{{- define "traefik.containerPorts" -}}
{{- range .Values.ports }}
- name: {{ .name }}
  containerPort: {{ .port }}
  hostPort: {{ .port }}
{{- end }}
{{- if .Values.admin.enable }}
- name: admin
  containerPort: {{ default 8080 .Values.admin.port }}
  hostPort: {{ default 8080 .Values.admin.port }}
{{- end }}
{{- end }}

{{/*
EntryPoints Options
*/}}
{{- define "traefik.entryPointsOptions" -}}
{{- range .options }}
- --entrypoints.{{ $.name }}.{{ . }}
{{- end }}
{{- end }}

{{/*
EntryPoints
*/}}
{{- define "traefik.entryPoints" -}}
{{- range .Values.ports }}
- --entrypoints.{{ .name }}.Address=:{{ .port }}
{{- include "traefik.entryPointsOptions" . }}
{{- end }}
{{- end }}

{{/*
Args
*/}}
{{- define "traefik.args" -}}
{{- range .Values.args }}
- {{ . }}
{{- end }}
{{- end }}

{{/*
Providers
*/}}
{{- define "traefik.providers" -}}
- --providers.kubernetescrd
{{- if .Values.certManager.enable }}
- --providers.kubernetesingress
{{- end }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "traefik.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "traefik.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
