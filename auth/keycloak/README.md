Example

---
apiVersion: helm.fluxcd.io/v1
kind: HelmRelease
metadata:
  name: keycloak
  namespace: auth
  annotations:
    flux.weave.works/automated: "false"
    flux.weave.works/tag.keycloak: glob:10.*
spec:
  releaseName: keycloak
  helmVersion: v3
  chart:
    git: ssh://git@bitbucket.org/valtechar/valtech-k8s-charts.git
    ref: master
    path: auth/keycloak
  values:
    namespace: auth
    
    keycloak:
      image: quay.io/keycloak/keycloak:10.0.1
      envvars:
        KEYCLOAK_USER: admin
        KEYCLOAK_PASSWORD: admin

```