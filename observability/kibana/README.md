Example

```
apiVersion: helm.fluxcd.io/v1
kind: HelmRelease
metadata:
  name: kibana
  namespace: monitoring
  annotations:
    flux.weave.works/automated: "false"
    flux.weave.works/tag.kibana: glob:7.*
spec:
  releaseName: kibana
  helmVersion: v3
  chart:
    git: ssh://git@bitbucket.org/valtechar/valtech-k8s-charts.git
    ref: master
    path: observability/kibana
  values:
    namespace: monitoring
    
    kibana:
      image: docker.elastic.co/kibana/kibana:7.2.0
```