Example

```
apiVersion: helm.fluxcd.io/v1
kind: HelmRelease
metadata:
  name: elasticsearch
  namespace: monitoring
  annotations:
    flux.weave.works/automated: "false"
    flux.weave.works/tag.elasticsearch: glob:7.*
spec:
  releaseName: elasticsearch
  helmVersion: v3
  chart:
    git: ssh://git@bitbucket.org/valtechar/valtech-k8s-charts.git
    ref: master
    path: observability/elasticsearch
  values:
    namespace: monitoring
    
    elasticsearch:
      image: docker.elastic.co/elasticsearch/elasticsearch:7.2.0
```