Example

```
apiVersion: helm.fluxcd.io/v1
kind: HelmRelease
metadata:
  name: fluentd
  namespace: monitoring
  annotations:
    flux.weave.works/automated: "false"
    flux.weave.works/tag.fluentd: glob:v1.4.2-debian-elasticsearch-1.*
spec:
  releaseName: fluentd
  helmVersion: v3
  chart:
    git: ssh://git@bitbucket.org/valtechar/valtech-k8s-charts.git
    ref: master
    path: observability/fluentd
  values:
    namespace: monitoring
    
    fluentd:
      image: fluent/fluentd-kubernetes-daemonset:v1.4.2-debian-elasticsearch-1.1
```